#ifndef _POSIX_SOURCE
#define _POSIX_SOURCE   /* For getaddrinfo(3) */
#endif

#ifndef _BSD_SOURCE
#define _BSD_SOURCE   /* For NI_MAXHOST up to glibc-2.19 */
#endif
#ifndef _DEFAULT_SOURCE
#define _DEFAULT_SOURCE   /* For NI_MAXHOST since glibc-2.20 */
#endif

#ifndef _XOPEN_SOURCE
#define _XOPEN_SOURCE 600   /* For unsetenv(3) */
#endif

#include "../test.h"
#include "test_DbOwnerInfo.h"
#include "server.h"
#include "isds.h"

static const char *username = "Doug1as$";
static const char *password = "42aA#bc8";


static int test_login(const isds_error error, struct isds_ctx *context,
        const char *url, const char *username, const char *password,
        const struct isds_pki_credentials *pki_credentials,
        struct isds_otp *otp) {
    isds_error err;

    err = isds_login(context, url, username, password, pki_credentials, otp);
    if (error != err)
        FAIL_TEST("Wrong return code: expected=%s, returned=%s (%s)",
                isds_strerror(error), isds_strerror(err),
                isds_long_message(context));

    PASS_TEST;
}

static int test_isds_FindPersonalDataBox(const isds_error expected_error,
        struct isds_ctx *context,
        const struct isds_DbOwnerInfo *criteria,
        const struct isds_list *expected_results) {
    isds_error error;
    struct isds_list *results;
    TEST_CALLOC(results);

    error = isds_FindPersonalDataBox(context, criteria, &results);
    TEST_DESTRUCTOR((void(*)(void*))isds_list_free, &results);

    if (expected_error != error) {
        FAIL_TEST("Wrong return code: expected=%s, returned=%s (%s)",
                isds_strerror(expected_error), isds_strerror(error),
                isds_long_message(context));
    }

    if (IE_SUCCESS != error && IE_2BIG != error) {
        TEST_POINTER_IS_NULL(results);
        PASS_TEST;
    }

    if (compare_isds_DbOwnerInfo_lists(expected_results, results))
        return 1;

    
    PASS_TEST;
}

int main(void) {
    int error;
    pid_t server_process;
    struct isds_ctx *context = NULL;

    INIT_TEST("isds_FindPersoanlDataBox");

    if (unsetenv("http_proxy")) {
        ABORT_UNIT("Could not remove http_proxy variable from environment\n");
    }
    if (isds_init()) {
        isds_cleanup();
        ABORT_UNIT("isds_init() failed\n");
    }
    context = isds_ctx_create();
    if (!context) {
        isds_cleanup();
        ABORT_UNIT("isds_ctx_create() failed\n");
    }

    {
        /* Full response with two results */
        char *url = NULL;

        struct isds_PersonName criteria_person_name = {
            .pnFirstName = "CN1",
            .pnMiddleName = "CN2",
            .pnLastName = "CN3",
            .pnLastNameAtBirth = NULL
        };
        struct tm criteria_biDate = {
            .tm_year = 4,
            .tm_mon = 5,
            .tm_mday = 6
        };
        struct isds_BirthInfo criteria_birth_info = {
            .biDate = &criteria_biDate,
            .biCity = "CB1",
            .biCounty = "CB2",
            .biState = "CB3"
        };
        long int criteria_adCode = 1;
        struct isds_Address criteria_address = {
            .adCode = &criteria_adCode,
            .adCity = "CA1",
            .adDistrict = "CA2",
            .adStreet = "CA3",
            .adNumberInStreet = "CA4",
            .adNumberInMunicipality = "CA5",
            .adZipCode = "CA6",
            .adState = "CA7"
        };
        _Bool criteria_aifoIsds = 1;
        struct isds_DbOwnerInfo criteria = {
            .dbID = "Cfoo123",
            .dbType = NULL,
            .ic = NULL,
            .personName = &criteria_person_name,
            .firmName = "C2",
            .birthInfo = &criteria_birth_info,
            .address = &criteria_address,
            .nationality = "C3",
            .email = NULL,
            .telNumber = NULL,
            .identifier = NULL,
            .aifoIsds = &criteria_aifoIsds,
            .registryCode = NULL,
            .dbState = NULL,
            .dbEffectiveOVM = NULL,
            .dbOpenAddressing = NULL
        };
        struct server_owner_info server_criteria = {
            .dbID = criteria.dbID,
            .aifoIsds = criteria.aifoIsds,
            .dbType = NULL,
            .ic = NULL,
            .pnFirstName = criteria.personName->pnFirstName,
            .pnMiddleName = criteria.personName->pnMiddleName,
            .pnLastName = criteria.personName->pnLastName,
            .pnLastNameAtBirth = NULL,
            .firmName = NULL,
            .biDate = criteria.birthInfo->biDate,
            .biCity = criteria.birthInfo->biCity,
            .biCounty = criteria.birthInfo->biCounty,
            .biState = criteria.birthInfo->biState,
            .adCode = criteria.address->adCode,
            .adCity = criteria.address->adCity,
            .adDistrict = criteria.address->adDistrict,
            .adStreet = criteria.address->adStreet,
            .adNumberInStreet = criteria.address->adNumberInStreet,
            .adNumberInMunicipality = criteria.address->adNumberInMunicipality,
            .adZipCode = criteria.address->adZipCode,
            .adState = criteria.address->adState,
            .nationality = criteria.nationality,
            .email = NULL,
            .telNumber = NULL,
            .identifier = NULL,
            .registryCode = NULL,
            .dbState = NULL,
            .dbEffectiveOVM = NULL,
            .dbOpenAddressing = NULL
        };

        struct isds_PersonName person_name = {
            .pnFirstName = "N1",
            .pnMiddleName = "N2",
            .pnLastName = "N3",
            .pnLastNameAtBirth = NULL
        };
        struct tm biDate = {
            .tm_year = 1,
            .tm_mon = 2,
            .tm_mday = 3
        };
        struct isds_BirthInfo birth_info = {
            .biDate = &biDate,
            .biCity = "B1",
            .biCounty = "B2",
            .biState = "B3"
        };
        long int adCode = 1;
        struct isds_Address address = {
            .adCode = &adCode,
            .adCity = "A1",
            .adDistrict = "A2",
            .adStreet = "A3",
            .adNumberInStreet = "A4",
            .adNumberInMunicipality = "A5",
            .adZipCode = "A6",
            .adState = "A7"
        };
        _Bool aifo_isds = 1;
        struct isds_DbOwnerInfo result = {
            .dbID = "foo1234",
            .dbType = NULL,
            .ic = NULL,
            .personName = &person_name,
            .firmName = NULL,
            .birthInfo = &birth_info,
            .address = &address,
            .nationality = "3",
            .email = NULL,
            .telNumber = NULL,
            .identifier = NULL,
            .aifoIsds = &aifo_isds,
            .registryCode = NULL,
            .dbState = NULL,
            .dbEffectiveOVM = NULL,
            .dbOpenAddressing = NULL
        };
        struct isds_DbOwnerInfo result2 = {
            0
        };
        struct server_owner_info server_result = {
            .dbID = result.dbID,
            .aifoIsds = result.aifoIsds,
            .dbType = NULL,
            .ic = NULL,
            .pnFirstName = result.personName->pnFirstName,
            .pnMiddleName = result.personName->pnMiddleName,
            .pnLastName = result.personName->pnLastName,
            .pnLastNameAtBirth = NULL,
            .firmName = NULL,
            .biDate = result.birthInfo->biDate,
            .biCity = result.birthInfo->biCity,
            .biCounty = result.birthInfo->biCounty,
            .biState = result.birthInfo->biState,
            .adCode = result.address->adCode,
            .adCity = result.address->adCity,
            .adDistrict = result.address->adDistrict,
            .adStreet = result.address->adStreet,
            .adNumberInStreet = result.address->adNumberInStreet,
            .adNumberInMunicipality = result.address->adNumberInMunicipality,
            .adZipCode = result.address->adZipCode,
            .adState = result.address->adState,
            .nationality = result.nationality,
            .email_exists = 0,
            .email = NULL,
            .telNumber_exists = 0,
            .telNumber = NULL,
            .identifier = NULL,
            .registryCode = NULL,
            .dbState = NULL,
            .dbEffectiveOVM = NULL,
            .dbOpenAddressing = NULL
        };
        struct server_owner_info server_result2 = {
            0
        };
        struct isds_list results2 = {
            .next = NULL,
            .data = &result2,
            .destructor = NULL
        };
        struct isds_list results = {
            .next = &results2,
            .data = &result,
            .destructor = NULL
        };
        struct server_list server_results2 = {
            .next = NULL,
            .data = &server_result2,
            .destructor = NULL
        };
        struct server_list server_results = {
            .next = &server_results2,
            .data = &server_result,
            .destructor = NULL
        };

        const struct arguments_DS_df_FindPersonalDataBox service_arguments = {
            .status_code = "0000",
            .status_message = "Ok.",
            .criteria = &server_criteria,
            .results_exists = 0,
            .results = &server_results
        };
        const struct service_configuration services[] = {
            { SERVICE_DS_Dz_DummyOperation, NULL },
            { SERVICE_DS_df_FindPersonalDataBox, &service_arguments },
            { SERVICE_END, NULL }
        };
        const struct arguments_basic_authentication server_arguments = {
            .username = username,
            .password = password,
            .isds_deviations = 1,
            .services = services
        };
        error = start_server(&server_process, &url,
                server_basic_authentication, &server_arguments, NULL);
        if (error == -1) {
            isds_ctx_free(&context);
            isds_cleanup();
            ABORT_UNIT(server_error);
        }
        TEST("login", test_login, IE_SUCCESS,
                context, url, username, password, NULL, NULL);
        free(url);

        TEST("All data", test_isds_FindPersonalDataBox, IE_SUCCESS,
                context, &criteria, &results);

        isds_logout(context);
        if (stop_server(server_process)) {
            isds_ctx_free(&context);
            isds_cleanup();
            ABORT_UNIT(server_error);
        }
    }

    {
        /* Truncated response with one result */
        char *url = NULL;

        struct isds_DbOwnerInfo criteria = {
            .dbID = "Cfoo123"
        };
        struct server_owner_info server_criteria = {
            .dbID = criteria.dbID
        };

        struct isds_PersonName person_name = {
            .pnFirstName = "N1",
            .pnMiddleName = "N2",
            .pnLastName = "N3",
            .pnLastNameAtBirth = NULL
        };
        struct tm biDate = {
            .tm_year = 1,
            .tm_mon = 2,
            .tm_mday = 3
        };
        struct isds_BirthInfo birth_info = {
            .biDate = &biDate,
            .biCity = "B1",
            .biCounty = "B2",
            .biState = "B3"
        };
        long int adCode = 4;
        struct isds_Address address = {
            .adCode = &adCode,
            .adCity = "A1",
            .adDistrict = "A2",
            .adStreet = "A3",
            .adNumberInStreet = "A4",
            .adNumberInMunicipality = "A5",
            .adZipCode = "A6",
            .adState = "A7"
        };
        _Bool aifoIsds = 1;
        struct isds_DbOwnerInfo result = {
            .dbID = "foo1234",
            .aifoIsds = &aifoIsds,
            .personName = &person_name,
            .birthInfo = &birth_info,
            .address = &address,
            .nationality = "3"
        };
        struct server_owner_info server_result = {
            .dbID = result.dbID,
            .aifoIsds = result.aifoIsds,
            .dbType = NULL,
            .ic = NULL,
            .pnFirstName = result.personName->pnFirstName,
            .pnMiddleName = result.personName->pnMiddleName,
            .pnLastName = result.personName->pnLastName,
            .pnLastNameAtBirth = NULL,
            .firmName = NULL,
            .biDate = result.birthInfo->biDate,
            .biCity = result.birthInfo->biCity,
            .biCounty = result.birthInfo->biCounty,
            .biState = result.birthInfo->biState,
            .adCode = result.address->adCode,
            .adCity = result.address->adCity,
            .adDistrict = result.address->adDistrict,
            .adStreet = result.address->adStreet,
            .adNumberInStreet = result.address->adNumberInStreet,
            .adNumberInMunicipality = result.address->adNumberInMunicipality,
            .adZipCode = result.address->adZipCode,
            .adState = result.address->adState,
            .nationality = result.nationality,
            .email_exists = 0,
            .email = NULL,
            .telNumber_exists = 0,
            .telNumber = NULL,
            .identifier = NULL,
            .registryCode = NULL,
            .dbState = NULL,
            .dbEffectiveOVM = NULL,
            .dbOpenAddressing = NULL
        };
        struct isds_list results = {
            .next = NULL,
            .data = &result,
            .destructor = NULL
        };
        struct server_list server_results = {
            .next = NULL,
            .data = &server_result,
            .destructor = NULL
        };

        const struct arguments_DS_df_FindDataBox service_arguments = {
            .status_code = "0003",
            .status_message = "Answer was truncated",
            .criteria = &server_criteria,
            .results_exists = 0,
            .results = &server_results
        };
        const struct service_configuration services[] = {
            { SERVICE_DS_Dz_DummyOperation, NULL },
            { SERVICE_DS_df_FindPersonalDataBox, &service_arguments },
            { SERVICE_END, NULL }
        };
        const struct arguments_basic_authentication server_arguments = {
            .username = username,
            .password = password,
            .isds_deviations = 1,
            .services = services
        };
        error = start_server(&server_process, &url,
                server_basic_authentication, &server_arguments, NULL);
        if (error == -1) {
            isds_ctx_free(&context);
            isds_cleanup();
            ABORT_UNIT(server_error);
        }
        TEST("login", test_login, IE_SUCCESS,
                context, url, username, password, NULL, NULL);
        free(url);

        TEST("Truncated answer", test_isds_FindPersonalDataBox, IE_2BIG,
                context, &criteria, &results);

        isds_logout(context);
        if (stop_server(server_process)) {
            isds_ctx_free(&context);
            isds_cleanup();
            ABORT_UNIT(server_error);
        }
    }

    {
        /* Report 0002 server error as IE_NOEXIST */
        char *url = NULL;

        struct isds_DbOwnerInfo criteria = {
            .dbID = "Cfoo123"
        };
        struct server_owner_info server_criteria = {
            .dbID = criteria.dbID
        };

        const struct arguments_DS_df_FindPersonalDataBox service_arguments = {
            .status_code = "0002",
            .status_message = "No such box",
            .criteria = &server_criteria,
            .results_exists = 0,
            .results = NULL
        };
        const struct service_configuration services[] = {
            { SERVICE_DS_Dz_DummyOperation, NULL },
            { SERVICE_DS_df_FindPersonalDataBox, &service_arguments },
            { SERVICE_END, NULL }
        };
        const struct arguments_basic_authentication server_arguments = {
            .username = username,
            .password = password,
            .isds_deviations = 1,
            .services = services
        };
        error = start_server(&server_process, &url,
                server_basic_authentication, &server_arguments, NULL);
        if (error == -1) {
            isds_ctx_free(&context);
            isds_cleanup();
            ABORT_UNIT(server_error);
        }
        TEST("login", test_login, IE_SUCCESS,
                context, url, username, password, NULL, NULL);
        free(url);

        TEST("Report 0002 server error as IE_NOEXIST",
                test_isds_FindPersonalDataBox, IE_NOEXIST, context, &criteria,
                NULL);

        isds_logout(context);
        if (stop_server(server_process)) {
            isds_ctx_free(&context);
            isds_cleanup();
            ABORT_UNIT(server_error);
        }
    }

    {
        /* Report 5001 server error as IE_NOEXIST */
        char *url = NULL;

        struct isds_DbOwnerInfo criteria = {
            .dbID = "Cfoo123"
        };
        struct server_owner_info server_criteria = {
            .dbID = criteria.dbID
        };

        const struct arguments_DS_df_FindPersonalDataBox service_arguments = {
            .status_code = "5001",
            .status_message = "No such box",
            .criteria = &server_criteria,
            .results_exists = 0,
            .results = NULL
        };
        const struct service_configuration services[] = {
            { SERVICE_DS_Dz_DummyOperation, NULL },
            { SERVICE_DS_df_FindPersonalDataBox, &service_arguments },
            { SERVICE_END, NULL }
        };
        const struct arguments_basic_authentication server_arguments = {
            .username = username,
            .password = password,
            .isds_deviations = 1,
            .services = services
        };
        error = start_server(&server_process, &url,
                server_basic_authentication, &server_arguments, NULL);
        if (error == -1) {
            isds_ctx_free(&context);
            isds_cleanup();
            ABORT_UNIT(server_error);
        }
        TEST("login", test_login, IE_SUCCESS,
                context, url, username, password, NULL, NULL);
        free(url);

        TEST("Report 0002 server error as IE_NOEXIST",
                test_isds_FindPersonalDataBox, IE_NOEXIST, context, &criteria,
                NULL);

        isds_logout(context);
        if (stop_server(server_process)) {
            isds_ctx_free(&context);
            isds_cleanup();
            ABORT_UNIT(server_error);
        }
    }




    isds_ctx_free(&context);
    isds_cleanup();
    SUM_TEST();
}
