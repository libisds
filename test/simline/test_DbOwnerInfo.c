#include "../test.h"
#include "isds.h"

static int compare_isds_PersonName(
        const struct isds_PersonName *expected_result,
        const struct isds_PersonName *result) {

    TEST_POINTER_DUPLICITY(expected_result, result);
    if (NULL == expected_result)
        return 0;

    TEST_STRING_DUPLICITY(expected_result->pnFirstName, result->pnFirstName);
    TEST_STRING_DUPLICITY(expected_result->pnMiddleName, result->pnMiddleName);
    TEST_STRING_DUPLICITY(expected_result->pnLastName, result->pnLastName);
    TEST_STRING_DUPLICITY(expected_result->pnLastNameAtBirth,
            result->pnLastNameAtBirth);

    return 0;
};

static int compare_isds_BirthInfo(
        const struct isds_BirthInfo *expected_result,
        const struct isds_BirthInfo *result) {

    TEST_POINTER_DUPLICITY(expected_result, result);
    if (NULL == expected_result)
        return 0;

    TEST_TMPTR_DUPLICITY(expected_result->biDate, result->biDate);
    TEST_STRING_DUPLICITY(expected_result->biCity, result->biCity);
    TEST_STRING_DUPLICITY(expected_result->biCounty, result->biCounty);
    TEST_STRING_DUPLICITY(expected_result->biState, result->biState);

    return 0;
};

static int compare_isds_Address(
        const struct isds_Address *expected_result,
        const struct isds_Address *result) {

    TEST_POINTER_DUPLICITY(expected_result, result);
    if (NULL == expected_result)
        return 0;

    TEST_INTPTR_DUPLICITY(expected_result->adCode, result->adCode);
    TEST_STRING_DUPLICITY(expected_result->adCity, result->adCity);
    TEST_STRING_DUPLICITY(expected_result->adDistrict, result->adDistrict);
    TEST_STRING_DUPLICITY(expected_result->adStreet, result->adStreet);
    TEST_STRING_DUPLICITY(expected_result->adNumberInStreet,
            result->adNumberInStreet);
    TEST_STRING_DUPLICITY(expected_result->adNumberInMunicipality,
            result->adNumberInMunicipality);
    TEST_STRING_DUPLICITY(expected_result->adZipCode, result->adZipCode);
    TEST_STRING_DUPLICITY(expected_result->adState, result->adState);

    return 0;
};


/* Compare isds_DbOwnerInfo.
 * @return 0 if equaled, 1 otherwise. */
static int compare_isds_DbOwnerInfo(
        const struct isds_DbOwnerInfo *expected_result,
        const struct isds_DbOwnerInfo *result) {

    TEST_POINTER_DUPLICITY(expected_result, result);
    if (NULL == expected_result)
        return 0;

    TEST_STRING_DUPLICITY(expected_result->dbID, result->dbID);
    TEST_INTPTR_DUPLICITY(expected_result->dbType, result->dbType);
    TEST_STRING_DUPLICITY(expected_result->ic, result->ic);
    if (compare_isds_PersonName(expected_result->personName,
                result->personName))
        return 1;
    TEST_STRING_DUPLICITY(expected_result->firmName, result->firmName);
    if (compare_isds_BirthInfo(expected_result->birthInfo, result->birthInfo))
        return 1;
    if (compare_isds_Address(expected_result->address, result->address))
        return 1;
    TEST_STRING_DUPLICITY(expected_result->nationality, result->nationality);
    TEST_STRING_DUPLICITY(expected_result->email, result->email);
    TEST_STRING_DUPLICITY(expected_result->telNumber, result->telNumber);
    TEST_STRING_DUPLICITY(expected_result->identifier, result->identifier);
    TEST_BOOLEANPTR_DUPLICITY(expected_result->aifoIsds, result->aifoIsds);
    TEST_STRING_DUPLICITY(expected_result->registryCode, result->registryCode);
    TEST_INTPTR_DUPLICITY(expected_result->dbState, result->dbState);
    TEST_BOOLEANPTR_DUPLICITY(expected_result->dbEffectiveOVM,
            result->dbEffectiveOVM);
    TEST_BOOLEANPTR_DUPLICITY(expected_result->dbOpenAddressing,
            result->dbOpenAddressing);

    return 0;
}

/* Compare list of isds_DbOwnerInfo structures.
 * @return 0 if equaled, 1 otherwise and set failure reason. */
int compare_isds_DbOwnerInfo_lists(const struct isds_list *expected_list,
        const struct isds_list *list) {
    const struct isds_list *expected_item, *item;
    int i;

    for (i = 1, expected_item = expected_list, item = list;
            NULL != expected_item && NULL != item;
            i++, expected_item = expected_item->next, item = item->next) {
        if (compare_isds_DbOwnerInfo(
                    (struct isds_DbOwnerInfo *)expected_item->data,
                    (struct isds_DbOwnerInfo *)item->data))
            return 1;
    }
    if (NULL != expected_item && NULL == item)
        FAIL_TEST("Result list is missing %d. item", i);
    if (NULL == expected_item && NULL != item)
        FAIL_TEST("Result list has superfluous %d. item", i);

    return 0;
}

