#ifndef __TEST_DBOWNERINFO_H
#define __TEST_DBOWNERINFO_H

#include <isds.h>

/* Compare list of isds_DbOwnerInfo structures.
 * @return 0 if equaled, 1 otherwise and set failure reason. */
int compare_isds_DbOwnerInfo_lists(const struct isds_list *expected_list,
        const struct isds_list *list);
#endif
